style_sheet: str = """
.main-window{
    background:#81BECE;
}

.left-panel{
    background: #036280;
    border-radius:10px;
}
.right-panel{
    background: #378BA4;
    border-radius:10px;
}
.left-panel QPushButton{
    border-radius: 10px;
    width:50px;
    height:50px;
    background-color:#E8EDE7;
    color: white;
}
.left-panel QPushButton:hover
{
  border: 2px solid #81BECE;
}
.left-panel QPushButton:pressed
{
  border: 2px solid gray;
  background-color:#E8EDf7; 
}

.right-panel QPushButton{
    border-radius: 10px;
    
    padding:5px;
    background-color:#E8EDE7; 
}
.right-panel QPushButton:pressed{
    background-color:#D8DDE7; 
    border: 2px solid gray;
}
.right-panel QPushButton:hover{
   
    border: 2px solid #81BECE;
}


QMenuBar {
    background-color: qlineargradient(
        x1:0, y1:0, x2:0, y2:1,
        stop:0 lightgray, stop:1 darkgray
    );
    spacing: 3px; /* spacing between menu bar items */
}

QTableView::indicator:unchecked {
    background-color: #d7d6d5
}
QTableView QTableCornerButton::section {
    background: green;
    border: 2px outset red;
}
QTableView {
    selection-background-color: blue;
    font:20px Courier;
}
"""
