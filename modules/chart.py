import pandas
from matplotlib.backends.backend_qt import NavigationToolbar2QT
from matplotlib.backends.backend_qtagg import FigureCanvasQTAgg
from matplotlib.figure import Figure
from PyQt5.QtWidgets import QVBoxLayout, QWidget

from utils import get_icon


class MplCanvas(FigureCanvasQTAgg):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super().__init__(fig)


class UserBalanceChart(QWidget):
    def __init__(self, username: str, bets: pandas.DataFrame):
        super().__init__()
        self.setWindowIcon(get_icon("window"))
        self.username = username
        self.bets = bets
        self._initialize_ui()

    def _initialize_ui(self):
        self.setMinimumSize(600, 400)
        self.setWindowTitle(f"Graph for {self.username} - {len(self.bets)} bets")
        self._setup_chart()
        self.show()

    def _setup_chart(self):
        chart = MplCanvas(self, width=5, height=4, dpi=100)
        chart.axes.plot(self.bets["datetime"], self.bets["balance"])
        chart.axes.set_title("User Balance dynamic")
        chart.axes.grid()
        if len(self.bets["datetime"]) > 1:
            chart.axes.set_xticks(
                [self.bets["datetime"][0], self.bets["datetime"].iat[-1]]
            )

        toolbar = NavigationToolbar2QT(chart, self)
        v_box = QVBoxLayout()
        v_box.addWidget(chart)
        v_box.addWidget(toolbar)
        self.setLayout(v_box)
