from datetime import datetime

import pandas
from PyQt5 import QtGui, QtWidgets
from PyQt5.QtCore import QObject, QThread, pyqtSignal
from PyQt5.QtWidgets import QLabel, QPushButton, QVBoxLayout, QWidget

from modules.chart import UserBalanceChart
from modules.observer import Event, EventTypes, Observable, Observer


class DataLoadSignals(QObject):
    finished = pyqtSignal()
    error = pyqtSignal(str)
    result = pyqtSignal(dict)


class AccountsDataLoadWorker(QThread):
    def __init__(self, userview):
        super().__init__()
        self.signals = DataLoadSignals()
        self.userview = userview

    def run(self):
        try:
            accounts = self.userview.data_source.get_accounts()
            result = {"accounts": accounts}
        except Exception as e:
            self.signals.error.emit(str(e))
        else:
            self.signals.finished.emit()
            self.signals.result.emit(result)


class BetsDataLoadWorker(QThread):
    def __init__(self, user_id, user_name, userview):
        super().__init__()
        self.signals = DataLoadSignals()
        self.userview = userview
        self.user_id = user_id
        self.user_name = user_name

    def run(self):
        try:
            bets = self.userview.data_source.get_bets_by_user_id(self.user_id)

            result = {"bets": bets, "username": self.user_name}
        except Exception as e:
            self.signals.error.emit(str(e))
        else:
            self.signals.finished.emit()
            self.signals.result.emit(result)


class UserView(QWidget, Observable):
    def __init__(self, main_window, parent, get_datasource, observer: Observer):
        super().__init__(parent)
        self.db_get = get_datasource
        self.data_source = self.db_get()
        self.main_window = main_window
        self.observer = observer
        self.observer.append(self)
        self.is_accounts_loading = False
        self.is_bets_loading = False

        self._setup_layout()

    def react_on(self, event: Event):
        """reaction from observer on outer events"""
        if event.e_type == EventTypes.DB_TYPE_CHANGED:
            self.data_source = self.db_get()
            self._start_accounts_load()

    def _setup_layout(self):
        layout = QVBoxLayout(self)
        self.label = QLabel("<h1>Accounts</h1>", self)
        self.updateListBtn = QPushButton("Refresh Accounts List", self)
        self.updateListBtn.clicked.connect(self._start_accounts_load)

        table_headers = [
            "ID",
            "First Name",
            "Bets Count",
        ]
        self.model = QtGui.QStandardItemModel()
        self.model.setHorizontalHeaderLabels(table_headers)
        self.table_view = QtWidgets.QTableView(self)
        self.table_view.setSizeAdjustPolicy(
            QtWidgets.QAbstractScrollArea.AdjustToContentsOnFirstShow
        )
        self.table_view.horizontalHeader().setStretchLastSection(True)
        self.table_view.setModel(self.model)
        self.table_view.setEditTriggers(QtWidgets.QTableView.EditTriggers())
        # self.table_view.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.model.setRowCount(0)
        self.model.setColumnCount(3)
        self.table_view.doubleClicked.connect(self._start_bets_load)
        self._start_accounts_load()

        layout.addWidget(self.label)
        layout.addWidget(self.table_view)
        layout.addWidget(self.updateListBtn)

    def _start_accounts_load(self):
        if self.is_accounts_loading:
            return
        self.is_accounts_loading = True
        self.observer.emit(
            Event(EventTypes.USERS_UPDATE_STARTED, "Accounts update start")
        )
        self.accounts_loader = AccountsDataLoadWorker(self)
        self.accounts_loader.signals.result.connect(self._on_account_load_result)
        self.accounts_loader.signals.finished.connect(self._on_accounts_load_finish)
        self.accounts_loader.signals.error.connect(self._on_accounts_load_fail)
        self.accounts_loader.start()

    def _on_account_load_result(self, result):
        self.model.clear()
        if result["accounts"] is None:
            return
        for account in result["accounts"]:
            _id = QtGui.QStandardItem(str(account.id))
            name = QtGui.QStandardItem(account.username)
            bet_count = QtGui.QStandardItem(str(account.bet_count))
            self.model.appendRow([_id, name, bet_count])

    def _on_accounts_load_finish(self):
        self.is_accounts_loading = False
        now = datetime.now().strftime("%H:%M:%S (%Y-%m-%d)")
        self.observer.emit(
            Event(
                EventTypes.USERS_UPDATED,
                f"Accounts update finished at {now}",
            )
        )

    def _on_accounts_load_fail(self, message):
        self.is_accounts_loading = False
        self.observer.emit(
            Event(
                EventTypes.USERS_UPDATE_FAIL,
                f"Accounts update Fail {message}",
            )
        )

    def _start_bets_load(self, item):
        if self.is_bets_loading:
            return
        self.is_bets_loading = True
        self.observer.emit(Event(EventTypes.BETS_LOAD_STARTED, "Bets load started"))
        user_id = self.model.index(item.row(), 0).data()
        user_name = self.model.index(item.row(), 1).data()

        self.bets_loader = BetsDataLoadWorker(user_id, user_name, self)
        self.bets_loader.signals.result.connect(self._show_balance_chart)
        self.bets_loader.signals.finished.connect(self._on_bets_load_finished)
        self.bets_loader.signals.error.connect(self._on_bets_load_error)
        self.bets_loader.start()

    def _on_bets_load_finished(self):
        self.is_bets_loading = False
        self.observer.emit(Event(EventTypes.BETS_LOAD_FINISHED, "Bets load finished"))

    def _on_bets_load_error(self, err):
        self.is_bets_loading = False
        self.observer.emit(Event(EventTypes.BETS_LOAD_FAIL, str(err)))

    def _show_balance_chart(self, result):
        bets_frame = pandas.DataFrame(result["bets"], columns=["balance", "datetime"])
        self.chart = UserBalanceChart(result["username"], bets_frame)
        self.chart.show()
