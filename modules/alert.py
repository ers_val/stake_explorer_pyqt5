from PyQt5.QtWidgets import QDialog, QLabel, QVBoxLayout


class AlertMessage(QDialog):
    def __init__(self, parent, text):
        super().__init__(parent)
        self.setWindowTitle("Alert!")
        self.layout = QVBoxLayout(self)
        message = QLabel(text)
        self.layout.addWidget(message)
