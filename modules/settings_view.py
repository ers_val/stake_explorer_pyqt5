from PyQt5 import QtWidgets

from data.db_types import DBTypes
from modules.observer import Event, EventTypes, Observable, Observer


class SettingsFields:
    MONGO_URL = "mongo_url"
    MONGO_DB = "mongo_db_name"
    MOCKDB_ACCOUNTS_PATH = "mockdb_accounts_json_path"
    MOCKDB_BETS_PATH = "mockdb_bets_json_path"
    LOG_PATH = "log_path"
    DB_TYPE = "db_type"


class SettingsView(QtWidgets.QWidget, Observable):
    def __init__(self, parent, settings_obj, observer: Observer):
        super().__init__(parent)
        self.settings = settings_obj
        self.observer = observer
        self.observer.append(self)
        self._setup_layout()

    def _setup_layout(self):
        setting_vbox = QtWidgets.QVBoxLayout(self)
        # settings_fbox = QtWidgets.QFormLayout(self)
        label = QtWidgets.QLabel("<h1>Settings</h1>")
        setting_vbox.addWidget(label)
        common_gb = QtWidgets.QGroupBox("Common")
        common_fbox = QtWidgets.QFormLayout(common_gb)

        mongo_gb = QtWidgets.QGroupBox("MongoDB")
        mongo_fbox = QtWidgets.QFormLayout(mongo_gb)

        mockdb_gb = QtWidgets.QGroupBox("MockDB")
        mockdb_fbox = QtWidgets.QFormLayout(mockdb_gb)
        setting_vbox.addWidget(common_gb)
        setting_vbox.addWidget(mongo_gb)
        setting_vbox.addWidget(mockdb_gb)

        self.db_type = QtWidgets.QComboBox()
        self.db_type.addItems(
            [DBTypes.MockDB, DBTypes.MongoDB, DBTypes.Postgres, DBTypes.SQlite]
        )
        self.mongo_url = QtWidgets.QLineEdit()
        self.mongo_url.setText(self.settings.value(SettingsFields.MONGO_URL))
        self.mongo_db_name = QtWidgets.QLineEdit()
        self.mongo_db_name.setText(self.settings.value(SettingsFields.MONGO_DB))

        self.mockdb_acc = QtWidgets.QLineEdit()
        self.mockdb_acc_btn = QtWidgets.QPushButton("Locate file")
        self.mockdb_bets = QtWidgets.QLineEdit()
        self.mockdb_bets_btn = QtWidgets.QPushButton("Locate file")

        self.log_file = QtWidgets.QLineEdit()
        self.log_file_btn = QtWidgets.QPushButton("Locate file")
        self.saveBtn = QtWidgets.QPushButton("Save")

        common_fbox.addRow("&Choose DB type:", self.db_type)
        mongo_fbox.addRow("&Type url to mongo:", self.mongo_url)
        mongo_fbox.addRow("&Type mongo db name:", self.mongo_db_name)
        mockdb_fbox.addRow("&Mock Account JSON:", self.mockdb_acc)
        mockdb_fbox.addRow("", self.mockdb_acc_btn)
        mockdb_fbox.addRow("&Mock Bets JSON:", self.mockdb_bets)
        mockdb_fbox.addRow("", self.mockdb_bets_btn)
        common_fbox.addRow("&Log file path:", self.log_file)
        common_fbox.addRow("", self.log_file_btn)
        setting_vbox.addWidget(self.saveBtn)
        self._setup_actions()
        self._read_settings()

    def _setup_actions(self):
        self.mockdb_acc_btn.clicked.connect(self._choose_accounts_json)
        self.mockdb_bets_btn.clicked.connect(self._choose_bets_json)
        self.log_file_btn.clicked.connect(self._choose_log_file)
        self.saveBtn.clicked.connect(self._save_settings)

    def _read_settings(self):
        self.db_type.setCurrentText(self.settings.value(SettingsFields.DB_TYPE))
        self.mongo_url.setText(self.settings.value(SettingsFields.MONGO_URL))
        self.mongo_db_name.setText(self.settings.value(SettingsFields.MONGO_DB))
        self.mockdb_acc.setText(
            self.settings.value(SettingsFields.MOCKDB_ACCOUNTS_PATH)
        )
        self.mockdb_bets.setText(self.settings.value(SettingsFields.MOCKDB_BETS_PATH))
        self.log_file.setText(self.settings.value(SettingsFields.LOG_PATH))

    def _save_settings(self):
        try:
            self.observer.emit(Event(EventTypes.SETTINGS_SAVING, "Saving Settings"))
            self.settings.setValue(SettingsFields.DB_TYPE, self.db_type.currentText())
            self.settings.setValue(SettingsFields.MONGO_URL, self.mongo_url.text())
            self.settings.setValue(SettingsFields.MONGO_DB, self.mongo_db_name.text())
            self.settings.setValue(
                SettingsFields.MOCKDB_ACCOUNTS_PATH, self.mockdb_acc.text()
            )
            self.settings.setValue(
                SettingsFields.MOCKDB_BETS_PATH, self.mockdb_bets.text()
            )
            self.settings.setValue(SettingsFields.LOG_PATH, self.log_file.text())
            self.observer.emit(Event(EventTypes.SETTINGS_SAVED, "Settings Updated"))
            self.observer.emit(Event(EventTypes.DB_TYPE_CHANGED, "DB_type changed"))
        except Exception as e:
            self.observer.emit(Event(EventTypes.SETTINGS_SAVE_FAILED, str(e)))

    def _choose_accounts_json(self):
        path, _ = QtWidgets.QFileDialog().getOpenFileName(
            None, "Choose Accounts JSON File", "./json", "*.json"
        )
        self.mockdb_acc.setText(path)

    def _choose_bets_json(self):
        path, _ = QtWidgets.QFileDialog().getOpenFileName(
            None, "Choose Bets JSON File", "./json", "*.json"
        )
        self.mockdb_bets.setText(path)

    def _choose_log_file(self):
        path, _ = QtWidgets.QFileDialog().getOpenFileName(
            None, "Choose Log File", ".", "*.log"
        )
        self.log_file.setText(path)
