class EventTypes:
    DB_TYPE_CHANGED = 1
    SETTINGS_SAVING = 2
    SETTINGS_SAVED = 3
    SETTINGS_SAVE_FAILED = 4
    USERS_UPDATE_STARTED = 5
    USERS_UPDATED = 6
    USERS_UPDATE_FAIL = 7
    BETS_LOAD_STARTED = 8
    BETS_LOAD_FINISHED = 9
    BETS_LOAD_FAIL = 10


class Event:
    def __init__(self, e_type, message):
        self.e_type = e_type
        self.message = message


class Observer:
    def __init__(self):
        self.widgets = []

    def append(self, widget):
        self.widgets.append(widget)

    def emit(self, event):
        for widget in self.widgets:
            widget.react_on(event)


class Observable:
    def react_on(self, event):
        pass
