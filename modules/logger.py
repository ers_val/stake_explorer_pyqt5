import logging

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QPlainTextEdit


class QTextEditLogger(logging.Handler, QtCore.QObject):
    appendPlainText = QtCore.pyqtSignal(str)

    def __init__(self, parent):
        super().__init__()
        QtCore.QObject.__init__(self)
        self.widget = QPlainTextEdit(parent)
        self.widget.setReadOnly(True)
        self.widget.setFixedHeight(50)
        self.widget.setStyleSheet("background: #e3e3e3")
        self.appendPlainText.connect(self.widget.appendPlainText)

    def emit(self, record):
        msg = self.format(record)
        self.appendPlainText.emit(msg)


class LogWidget(QtWidgets.QWidget):
    def __init__(self):
        # log_label = QtWidgets.QLabel("Logs:", self)
        logTextBox = QTextEditLogger(self)
        logTextBox.setFormatter(
            logging.Formatter(
                "%(asctime)s %(levelname)s %(module)s %(funcName)s %(message)s"
            )
        )
        logging.getLogger().addHandler(logTextBox)
        logging.getLogger().setLevel(logging.INFO)
        # user_layout.addWidget(log_label)
        # user_layout.addWidget(logTextBox.widget)
