import ctypes
import logging
import sys

import matplotlib
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import (QApplication, QGraphicsDropShadowEffect,
                             QMainWindow, QWidget)

from data.db_types import DBTypes, DumpDB, MockDB, MongoDb
from modules.alert import AlertMessage
from modules.observer import Event, Observable, Observer
from modules.settings_view import SettingsFields, SettingsView
from modules.users_view import UserView
from style import style_sheet
from utils import get_icon, make_logfile

myappid = "mycompany.stake_explorer.subproduct.version"  # arbitrary string
ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)

matplotlib.use("Qt5Agg")
current_user = None


class MainWindow(QMainWindow, Observable):
    def _switch_page(self, stack, page):
        stack.setCurrentWidget(page)

    def _get_data_source(self):
        try:
            if not self.settings.value(SettingsFields.DB_TYPE):
                raise ValueError("No db type setting")
            if self.settings.value(SettingsFields.DB_TYPE) == DBTypes.MockDB:
                return MockDB(
                    self.settings.value(SettingsFields.MOCKDB_ACCOUNTS_PATH),
                    self.settings.value(SettingsFields.MOCKDB_BETS_PATH),
                )

            if self.settings.value(SettingsFields.DB_TYPE) == DBTypes.MongoDB:
                db_name = self.settings.value(SettingsFields.MONGO_DB)
                if db_name:
                    return MongoDb(
                        url=self.settings.value(SettingsFields.MONGO_URL, db_name)
                    )
                else:
                    return MongoDb(url=self.settings.value(SettingsFields.MONGO_URL))

        except Exception as e:
            alert = AlertMessage(
                self, f"Error: {str(e)}, Empty DumpDB was chosen as default DB"
            )
            alert.exec()
            return DumpDB()

    def _init_panels(self):
        # effects
        shadow = QGraphicsDropShadowEffect()
        shadow.setBlurRadius(5)
        shadow.setXOffset(0)
        shadow.setYOffset(0)
        shadow2 = QGraphicsDropShadowEffect()
        shadow2.setBlurRadius(-5)
        shadow2.setXOffset(0)
        shadow2.setYOffset(0)

        # main area
        panels = QtWidgets.QWidget(self.main_view)
        panels_hbox = QtWidgets.QHBoxLayout(panels)

        left_panel = QtWidgets.QWidget(panels)
        left_panel.setProperty("class", "left-panel")
        left_panel.setGraphicsEffect(shadow2)
        left_panel_vbox = QtWidgets.QVBoxLayout(left_panel)
        usersPageBtn = QtWidgets.QPushButton("", left_panel)
        usersIcon = QtGui.QPixmap("./static/users.png")
        usersPageBtn.setProperty("class", "left-panel")
        usersPageBtn.setIcon(QtGui.QIcon(usersIcon))

        settingsPageBtn = QtWidgets.QPushButton("", left_panel)
        settingsIcon = QtGui.QPixmap("./static/settings.png")
        settingsPageBtn.setIcon(QtGui.QIcon(settingsIcon))
        settingsPageBtn.setProperty("class", "left-panel")

        left_panel_vbox.addWidget(
            usersPageBtn, alignment=QtCore.Qt.AlignmentFlag.AlignTop
        )
        left_panel_vbox.addWidget(settingsPageBtn)

        right_panel = QtWidgets.QWidget()
        right_panel.setProperty("class", "right-panel")
        right_panel.setGraphicsEffect(shadow)
        right_panel_sbox = QtWidgets.QStackedLayout(right_panel)

        users_page: QWidget = UserView(
            self, right_panel, self._get_data_source, self.observer
        )
        settings_page: QWidget = SettingsView(right_panel, self.settings, self.observer)

        usersPageBtn.clicked.connect(
            lambda: self._switch_page(right_panel_sbox, users_page)
        )
        settingsPageBtn.clicked.connect(
            lambda: self._switch_page(right_panel_sbox, settings_page)
        )
        right_panel_sbox.addWidget(users_page)
        right_panel_sbox.addWidget(settings_page)

        panels_hbox.addWidget(left_panel)
        panels_hbox.addWidget(right_panel)
        return panels

    def _init_logger(self):
        self.logger = logging.getLogger("main")
        if self.settings.value(SettingsFields.LOG_PATH):
            f_handler = logging.FileHandler(
                self.settings.value(SettingsFields.LOG_PATH)
            )
            f_format = logging.Formatter(
                "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
            )
            f_handler.setFormatter(f_format)
            self.logger.addHandler(f_handler)
        else:
            c_handler = logging.StreamHandler()
            c_format = logging.Formatter("%(name)s - %(levelname)s - %(message)s")
            c_handler.setFormatter(c_format)
            self.logger.addHandler(c_handler)
        self.logger.setLevel(logging.INFO)

    def __init__(self):
        super().__init__()
        self.settings = QtCore.QSettings(
            "./settings.ini", QtCore.QSettings.Format.IniFormat
        )
        self._init_logger()
        self.threadpool = QtCore.QThreadPool()
        self.setWindowTitle("Stake Explorer")
        self.resize(600, 500)
        self.setWindowIcon(get_icon("main"))
        self.statusBar().showMessage("Welcome!")
        self.observer = Observer()
        self.observer.append(self)

        self.main_view = QtWidgets.QWidget()
        self.main_view.setProperty("class", "main-window")
        main_vbox = QtWidgets.QVBoxLayout(self.main_view)
        main_vbox.addWidget(self._init_panels())

        self.setCentralWidget(self.main_view)

    def react_on(self, event: Event):
        self.statusBar().showMessage(event.message)
        self.logger.info(event.message)


if __name__ == "__main__":
    make_logfile()
    app = QApplication(sys.argv)
    app.setStyleSheet(style_sheet)
    app.setWindowIcon(get_icon("main"))
    window = MainWindow()
    window.show()

    app.exec()
