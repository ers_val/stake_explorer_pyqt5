from dataclasses import dataclass, field
from datetime import datetime


def bet_result(sum_return, bet_amount):
    return sum_return - bet_amount


def balance(start_balance, return_sum, bet_amount):
    return start_balance + bet_result(return_sum, bet_amount)


@dataclass
class Account:
    _id: dict = field(default_factory=dict)
    id: str = ""
    username: str = ""
    password: str = ""
    status: str = ""
    start_dt: datetime = datetime.now()
    last_dt: datetime = datetime.now()
    win_lost: float = 0
    deposit: str = ""
    withdraw: str = ""
    staked: int = 0
    playtime: str = ""
    bet_count: int = 0
    bot_bet_count: int = 0
    difference: int = 0
    merged_bets_data: int = 0
    roi: float = 0
    flets: float = 0
    average_bet_amount: float = 0

    def __repr__(self):
        return self.username

    def __str__(self):
        return self.username


@dataclass
class Bet:
    _id: dict
    id: str
    bet_datetime: datetime
    markets: str
    odds: float
    bet_amount: int
    _return: int
    balance_first: float | int
    balance_second: int
    bet_type_first: str
    bet_type_second: str
    bookmaker_account_first: str
    bookmaker_account_second: str
    bookmaker_first: str
    bookmaker_second: str
    cf_first: float
    cf_second: float
    currency_first: str
    currency_second: str
    datetime_first: datetime
    datetime_second: datetime
    income: float
    league_first: str
    league_second: str
    sport: str
    stake_first: int
    stake_second: int
    teams_first: str
    teams_second: str

    @property
    def bet_result(self):
        return self._return - self.bet_amount

    @property
    def deposit(self):
        return self.balance_first - self.bet_result

    def __repr__(self):
        return f"{self.bookmaker_account_first} - {self.bet_datetime}"

    def __str__(self):
        return self.id
