import json
import logging
from abc import ABC, abstractmethod
from typing import Dict, List, Optional

import pandas
from pymongo import MongoClient

from data.data_types import Account, balance

logger = logging.getLogger("main")


def is_valid_bet_doc(doc):
    keys = ["id", "bet_datetime", "bet_amount", "return", "balance_first"]
    for key in keys:
        if key not in doc or doc[key] is None:
            logger.error(f"{doc['id']} key {key} problem")
            return False
    return True


def is_valid_acc_doc(doc):
    keys = ["id", "username", "staked"]
    for key in keys:
        if key not in doc or not doc[key]:
            logger.error(f"{doc['id']} key {key} problem")
            return False
    return True


class DBTypes:
    MockDB = "Mock"
    MongoDB = "Mongo"
    SQlite = "Sqlite"
    Postgres = "Postgres"


class DB(ABC):
    @abstractmethod
    def get_accounts(self) -> List[Account]:
        pass

    @abstractmethod
    def get_bets_by_user_id(self, user_id: str) -> Optional[pandas.DataFrame]:
        pass


class MongoDb(DB):
    def __init__(self, url, db_name="test_database"):
        self.client = MongoClient(url)
        self.db = self.client[db_name]

    def get_accounts(self) -> List[Account]:
        docs = list(
            self.db.accounts.find(
                {}, {"id": 1, "username": 1, "staked": 1, "bet_count": 1}
            )
        )
        accs = [
            Account(id=i["id"], username=i["username"], staked=i["staked"])
            for i in docs
        ]

        return accs

    def get_bets_by_user_id(self, user_id):
        docs = list(
            self.db.bets.find(
                {"id": int(user_id)},
                {
                    "bet_datetime": 1,
                    "return": 1,
                    "balance_first": 1,
                    "bet_amount": 1,
                    "id": 1,
                },
            )
        )
        user_bets_for_chart = [
            {
                "datetime": i.get("bet_datetime"),
                "balance": balance(i["balance_first"], i["return"], i["bet_amount"]),
            }
            for i in docs
            if is_valid_bet_doc(i)
        ]
        user_bets_for_chart.sort(key=lambda i: i["datetime"])
        return pandas.DataFrame(user_bets_for_chart, columns=["datetime", "balance"])


class MockDB(DB):
    def __init__(self, accounts_path, bets_path) -> None:
        self.__account_path = accounts_path
        self.__bet_path = bets_path

    def __get_json_data(self, path):
        with open(path, "r", encoding="utf-8") as file:
            return file.read()

    def get_accounts(self) -> List[Account]:
        json_data = self.__get_json_data(self.__account_path)
        data = json.loads(json_data)
        return [
            Account(
                i.get("_id"),
                i.get("id"),
                i.get("username"),
                i.get("password"),
                i.get("status"),
                i.get("start_dt"),
                i.get("last_dt"),
                i.get("win_lost"),
                i.get("deposit"),
                i.get("withdraw"),
                i.get("staked"),
                i.get("playtime"),
                i.get("bet_count"),
                i.get("bot_bet_count"),
                i.get("difference"),
                i.get("merged_bets_data"),
                i.get("roi"),
                i.get("flets"),
                i.get("average_bet_amount"),
            )
            for i in data
        ]

    def get_bets(self) -> List[Dict]:
        json_data = self.__get_json_data(self.__bet_path)
        data = json.loads(json_data)
        bets = [
            {
                "id": i.get("id"),
                "bet_datetime": i.get("bet_datetime"),
                "bet_amount": i.get("bet_amount", 0),
                "return": i.get("return", 0),
                "balance_first": i.get("balance_first"),
            }
            for i in data
        ]
        return bets

    def get_bets_by_user_id(self, user_id) -> Optional[pandas.DataFrame]:
        bets = self.get_bets()
        user_bets = list(filter(lambda i: i["id"] == user_id, bets))
        user_bets_for_chart = [
            {
                "datetime": i.get("bet_datetime"),
                "balance": balance(i["balance_first"], i["return"], i["bet_amount"]),
            }
            for i in user_bets
            if is_valid_bet_doc(i)
        ]

        user_bets_for_chart.sort(key=lambda i: i["datetime"])

        return pandas.DataFrame(user_bets_for_chart, columns=["datetime", "balance"])


class DumpDB(DB):
    def get_accounts(self):
        return []

    def get_bets_by_user_id(self):
        return []
