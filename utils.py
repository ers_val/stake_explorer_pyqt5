import os

from PyQt5 import QtGui


def get_icon(icon_type: str):
    if icon_type == "main":
        return QtGui.QIcon("./static/app_icon.ico")
    else:
        return QtGui.QIcon("./static/window_icon.ico")


def make_logfile():
    if os.path.exists("./logfile.log"):
        return
    file = open("./logfile.log", "w")
    file.close()
