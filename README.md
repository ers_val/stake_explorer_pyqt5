# Тестовое задание для программиста – Разработка PyQt приложения для анализа аккаунтов и ставок

### Задача:

Необходимо разработать приложение на PyQt,
которое позволит пользователям просматривать список аккаунтов,
а также анализировать динамику баланса пользователей на основе их ставок.

### Основные требования:

#### Интерфейс Приложения:

    Приложение должно отображать главное окно со списком аккаунтов в таблице.
    Должна быть реализована функция двойного клика на аккаунт в таблице, чтобы открыть новое окно с графиком динамики баланса выбранного аккаунта.

#### Работа с Данными:

    На основе id аккаунта из таблицы, программа должна уметь получать документы из коллекции bets и формировать из них pandas DataFrame.

    Необходимо создать две функции для работы с данными ставок:
    - Функция для расчёта результата каждой ставки (выигрыш(return) минус сумма ставки(bet_amount)).
    - Функция для расчёта текущего баланса пользователя после каждой ставки на основе предыдущего баланса и результата ставки.

    Визуализация Данных:
    На основе столбца balance из данных аккаунта необходимо построить график динамики изменения баланса.

### Технические требования:

- Язык программирования: Python 3.x.
- Графический интерфейс: PyQt5 или PyQt6.
- Обработка данных: pandas.
- Визуализация данных: matplotlib, plotly или аналогичная библиотека.

# Требования

- писалось на python 3.11, но вряд ли будут проблемы с версиями от 3.7+
- установить зависмости `pip install -r requirements.txt`
- запуск от python или pythonw
- функциониование монги тестировалось на базе "test_database" и коллекциях "accounts", "bets"
- первоначально потребуется выбрать тип базы json/mongo и добавить соответствующие настройки.
- формат url до монго базы 'mongodb+srv://<user>:<pass>@<host:port>/
